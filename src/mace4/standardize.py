#! /usr/bin/python3

# Post processing to standardize Mace4 outputs.
# This is to ensure Mace4 outputs across multiple versions can be compared.
# Some output formats may change over time, and execution times are usually
# not constant over time.  We are interested mostly in the output models
# and the search path lengths and some other statistics.


import argparse
from sys import stdin, stdout


INPUT_HEADING = "============================== INPUT"
OLD_INPUT_HEADING = "============================== INPUT =================================\n"
END_OF_INPUT = "============================== end of input"
OLD_END_OF_INPUT = "============================== INPUT =================================\n"
PROCESS_NON_CLAUSAL = "============================== PROCESS NON-CLAUSAL FORMULAS"
OLD_PROCESS_NON_CLAUSAL = "============================== PROCESS NON-CLAUSAL FORMULAS ==========\n"
END_OF_PROCESS_NON_CLAUSAL = "============================== end of process non-clausal formulas"
OLD_END_OF_PROCESS_NON_CLAUSAL = "============================== end of process non-clausal formulas ===\n"
CLAUSSES_FOR_SEARCH = "============================== CLAUSES FOR SEARCH"
OLD_CLAUSSES_FOR_SEARCH = "============================== CLAUSES FOR SEARCH ====================\n"
MACE_FORMULAS = "formulas(mace4_clauses)"
OLD_MACE_FORMULAS = "formulas(mace4_clauses).\n"
END_CLAUSSES_FOR_SEARCH = "============================== end of clauses for search"
OLD_END_CLAUSSES_FOR_SEARCH = "============================== end of clauses for search =============\n"
DOMAIN_SIZE = "============================== DOMAIN SIZE "
STATISTICS = "============================== STATISTICS"
OLD_STATISTICS = "============================== STATISTICS ============================\n"
END_STATISTICS = "============================== end of statistics"
OLD_END_STATISTICS = "============================== end of statistics =====================\n"
CURRENT_CPU = "Current CPU time:"
MODEL = "============================== MODEL"
OLD_MODEL = "============================== MODEL =================================\n"
END_OF_MODEL = "============================== end of model"
OLD_END_OF_MODEL = "============================== end of model ==========================\n"
INTERPRETATION = "interpretation( "
CURRENT_CPU_TIME = "Current CPU time"
USER_CPU = "User_CPU="
PROCESS = "Process "
THE_PROCESS = "The process finished "



def process_line(line):
    if line.startswith(END_OF_INPUT):
        return OLD_END_OF_INPUT
    elif line.startswith(PROCESS_NON_CLAUSAL):
        return OLD_PROCESS_NON_CLAUSAL
    elif line.startswith(END_OF_PROCESS_NON_CLAUSAL):
        return OLD_END_OF_PROCESS_NON_CLAUSAL
    elif line.startswith(CLAUSSES_FOR_SEARCH):
        return OLD_CLAUSSES_FOR_SEARCH
    elif line.startswith(MACE_FORMULAS):
        return OLD_MACE_FORMULAS
    elif line.startswith(END_CLAUSSES_FOR_SEARCH):
        return OLD_END_CLAUSSES_FOR_SEARCH
    elif line.startswith(DOMAIN_SIZE):
        return "\n"
    elif line.startswith(STATISTICS):
        return OLD_STATISTICS
    elif line.startswith(END_STATISTICS):
        return OLD_END_STATISTICS
    elif line.startswith(CURRENT_CPU):
        return "\n"
    elif line.startswith(MODEL):
        return OLD_MODEL
    elif line.startswith(INTERPRETATION):
        return line[0:25] + "\n"
    elif line.startswith(END_OF_MODEL):
        return OLD_END_OF_MODEL
    elif line.startswith(USER_CPU):
        return "\n"
    elif line.startswith(PROCESS):
        return "\n"
    elif line.startswith(THE_PROCESS):
        return "\n"
    elif line.startswith("    % assign("):
        return ""
    elif line.startswith("assign("):
        return ""
    elif line.startswith("    % set("):
        return ""
    elif line.startswith("% set("):
        return ""
    elif line.startswith("% Reading from file"):
        return ""
    elif "list(distinct)" in line:
        return "list(distinct).\n"
    elif "Declaring Mace4 arithmetic parse types" in line:
        return "    % Declaring Mace4 arithmetic parse types.\n"
    else:
        return line


def process_file(fin, fout):
    line = fin.readline()
    while len(line) > 0 and not line.startswith(INPUT_HEADING):
        line = fin.readline()
    if len(line) > 0:
        fout.write(OLD_INPUT_HEADING)
    line = fin.readline()
    while len(line) > 0:
        fout.write(process_line(line))
        line = fin.readline()
    
    
    
def main():
    
    parser = argparse.ArgumentParser(prog="standardize.py",
                                     usage='%(prog)s [options] --fin Mace4-output-file --fout Mace4-standardized-output-file',
                                     description='standardize Mace4 outputs')
    parser.add_argument('--fin', type=str, required=False, help='Input - the full file path name of Mace4 output file')
    parser.add_argument('--fout', type=str, required=False, help='Outpuyt - the full file path name of standardized Mace4 output file')

    args = parser.parse_args()
    
    if args.fin:
        fin = open(args.fin)
    else:
        fin = stdin
    if args.fout:
        fout = open(args.fout)
    else:
        fout = stdout

    process_file(fin, fout)

    if fin != stdin:
        fin.close()
    if fout != stdout:
        fout.close()


if __name__== "__main__":
    main()


    