#! /usr/bin/python3

# This scripts runs mace4 with a tree of input files (toghether with their corresponding output files)
# to generate the standardized output files, and also generate the standardized output files from the 
# saved referenced output files, then compare these standardized output files with the standardized 
# reference output file using diff --ignore-all-space.
# It may also generate the reference output files at the same time if the reference
# Mace4 executable is given, and the "regen" flag is set.

import sys
import os
import argparse
import re

from pathlib import Path
from configparser import SafeConfigParser
from subprocess import CalledProcessError, check_call, run


def splitall(path):
    """ Split a file path into separate folder names
    """
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts



def do_tests(working_dir, top_in_dir, regen, old_mace4, new_mace4, standardizer, black_list, white_list):
    """ Testing is done on the working dir, which does not change in the process.
        All pathnames are relative to this working dir.
        
        working_dir is the full path name of the directory of the run.  All temporary file are deposited here,
                    and all relative file paths are pivoted here
        top_in_dir is the root dir of the tree of input files (and their corresponding reference output files)
        regen is a boolean flag to tell whether reference output files are to be generated.
        old_mace4 is the Mace4 executable to use to generate the reference output files, if regen is True
        new_mace4 is the Mace4 executable to be tested
        standardizer is the python3 script that standardize the output files for easy comparison
        black_list is a list of file names and directory names to be excluded from the run
        white_list is the list of file names to be included in the run
        Only one of black_list and white_list is can be a non-empty list.
        
        The final results are printed on stdout.
    """
    results_file = 'all_diffs.txt'
    open(results_file, 'w').close()
    top_dir = os.path.basename(os.path.normpath(top_in_dir))
    top_in_dir_str = f"{top_in_dir}/"
    for dirName, _, fileList in os.walk(top_in_dir):
        dir_nodes = splitall(dirName)
        if any([x in black_list for x in dir_nodes]):
            continue
        if dirName == top_in_dir:
            sub_dir = ""
        else:
            sub_dir = dirName.replace(top_in_dir_str, "")

        new_working_path_dir = os.path.join(top_dir, sub_dir)
        Path(new_working_path_dir).mkdir(parents=True, exist_ok=True)

        # Process each file
        for file in fileList:
            if not file.endswith(".in"):
                continue
            if file in black_list:
                continue
            if len(white_list) > 0 and not file in white_list:
                continue

            in_file_path = os.path.join(dirName, file)
            out_file_name_base = file.replace(".in", ".out")
            old_out_file_path = os.path.join(dirName, out_file_name_base)
            new_out_file = os.path.join(new_working_path_dir, f"{out_file_name_base}.new")
            old_out_file = os.path.join(new_working_path_dir, f"{out_file_name_base}")

            print('.', end='', flush=True)
            if regen:
                try:
                    check_call(f"{old_mace4} -f {in_file_path} 2>/dev/null > {old_out_file_path}", shell=True)
                except CalledProcessError as e:
                    x = re.findall("returned non-zero exit status ([\d]+)\.", str(e))                    
                    if len(x) > 0 and 0 < int(x[0]) < 8:
                        pass
                    else:
                        raise
            
            check_call(f"{standardizer} --fin {old_out_file_path} > {old_out_file}", shell=True)
            check_call(f"{new_mace4} -f {in_file_path} 2>/dev/null | {standardizer} > {new_out_file} ", shell=True)
            with open(results_file, "a+") as fp:
                fp.writelines(f"\n{in_file_path}:")
            run(f"diff --ignore-all-space {old_out_file} {new_out_file} >> {results_file}", shell=True)
    
    print(f"\n\n\nFinal results in {working_dir}/{results_file}:\n")
    with open(results_file) as fp:
        print(fp.read())
    
   
def main():
    
    arg_parser = argparse.ArgumentParser(prog="compare.py",
                                         usage='%(prog)s --config .ini config-file',
                                         description='generate and compare Mace4 outputs')
    arg_parser.add_argument('--config', type=str, required=True, help='The full file path name of the .ini config file')

    args = arg_parser.parse_args()

    parser = SafeConfigParser()
    parser.read(args.config)

    working_dir = parser.get('mace_test', 'working_dir')
    Path(working_dir).mkdir(parents=True, exist_ok=True)
    os.chdir(working_dir)
    top_in_dir = parser.get('mace_test', 'top_input_dir')
    black_list = parser.get('mace_test', 'black_list')
    white_list = parser.get('mace_test', 'white_list')
    
    if white_list != "" and black_list != "":
        sys.exit("Error: Only one of white_list and black_list can be non-empty.")
    if white_list != "":
        white_list = white_list.split(",")
    if black_list != "":
        black_list = black_list.split(",")
    
    old_mace = parser.get('mace_test', 'old_mace')
    new_mace = parser.get('mace_test', 'new_mace')
    standardizer = parser.get('mace_test', 'standardize_script')
    
    regen = parser.getboolean('mace_test', 'regen')
    
    do_tests(working_dir, top_in_dir, regen, old_mace, new_mace, standardizer, black_list, white_list)
    


if __name__== "__main__":
    main()
