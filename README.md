# Mace4_testing

This Python3 project is to automate testing, especially regression tesing, for the Mace4 project. For example, you have updated the Mace4 code base and generated a new Mace4 executable, and you want to see how your new Mace4's outputs compare with those in the baseline. This script will run your new Mace4 executable on a list of input files and compare the output files against the existing output files. Some basic test input files and reference output files are provided in `.../mace4_testing/data/test_data/basic_tests`.

## Pre-requisites

The script is designed to run on Linux-like platforms, with Python 3.6 installed.  It uses `diff --ignore-all-space` for comparing files. It has been tested on Ubuntu 18.04 and Python 3.6.9.

## Running the Tests

In general, output files from different Mace4 runs on the same input file will not be identical because of time-specific outputs (e.g. run-time), and other environmental dependencies (such as process id, input file paths etc).  A script, `.../mace4_testing/src/mace4/standardize.py`, is used to mask out these differences so that a meaningful comparison can be done.

The test input files (e.g. semigroup.in) and reference output files (e.g. semigroup.out) are put under a hierachy of directories. The mace4 version to be tested is run to generate the output files (e.g. semigroup.out.new) and these new output files are post-processed to remove known run-time-dependent outputs, and are then compared against the reference output files (i.e. semigroup.out) for correctness. There is also an option to re-generate the "reference output files" if the reference version of mace4 is provided (all specified in the .ini config file).

The main script is `compare.py`. It takes, as command line argument, an .ini config file that specifies all input parameters such as the root directory of all test input files (input files must have suffix of .in), reference output files (must have the same base name as the input file and have suffix of .out), and working directory, etc. All options are documented in the sample config file `.../data/test.ini`. An sample run would look like:

    ./src/mace4/compare.py --config ./data/test.ini
    
The `compare.py` script walks through the tree of input files and run Mace4 on each of them to generate output files, which are then compared against the reference output files provided in the input tree.  

A sample .ini config file is provided in `.../mace4_testing/data/test.ini`.  You most likely need to update at least the `working_dir` to make it work in your platform.